package com.epam.view;

import com.epam.controller.config.FirstConfiguration;
import com.epam.controller.config.SecondConfiguration;
import com.epam.controller.config.ThirdConfiguration;
import com.epam.model.cars.Parking;
import com.epam.model.pojo.other.BeanGenerator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainView {
    public void run() {
        ApplicationContext context = new AnnotationConfigApplicationContext(
                FirstConfiguration.class,
                SecondConfiguration.class,
                ThirdConfiguration.class,
                Parking.class);
        context.getBean(Parking.class).printCarsEngines();
        context.getBean(BeanGenerator.class);

    }
}
