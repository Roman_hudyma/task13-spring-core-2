package com.epam.model.cars;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Parking {
    @Autowired
    private List<Car> cars;

    public void printCarsEngines() {
        for (Car car : cars) {
            System.out.println(car.getEngine());
        }
    }


}
