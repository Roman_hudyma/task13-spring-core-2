package com.epam.model.cars;

public interface Car {
    String getEngine();
}
