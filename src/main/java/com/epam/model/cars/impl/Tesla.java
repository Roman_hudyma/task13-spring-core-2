package com.epam.model.cars.impl;

import com.epam.model.cars.Car;
import org.springframework.stereotype.Component;

@Component
public class Tesla implements Car {
    @Override
    public String getEngine() {
        return "no such engine:)";
    }
}
