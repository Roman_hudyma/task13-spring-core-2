package com.epam.model.cars.impl;

import com.epam.model.cars.Car;
import org.springframework.stereotype.Component;

@Component
public class Toyota implements Car {
    @Override
    public String getEngine() {
        return "1500 cubes";
    }
}
