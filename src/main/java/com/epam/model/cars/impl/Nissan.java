package com.epam.model.cars.impl;

import com.epam.model.cars.Car;
import org.springframework.stereotype.Component;

@Component
public class Nissan implements Car {
    @Override
    public String getEngine() {
        return "2200 cubes";
    }
}
