package com.epam.model.pojo.beans1;

import org.springframework.stereotype.Component;

@Component
public class BeanB {
    private String name;
    private int value;

    public BeanB() {
        System.out.println("THIS IS BEANA");
    }

    public BeanB(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
