package com.epam.model.pojo.beans1;

import org.springframework.stereotype.Component;

@Component
public class BeanC {
    private String name;
    private int value;

    public BeanC() {
        System.out.println("THIS IS BEANA");
    }

    public BeanC(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
