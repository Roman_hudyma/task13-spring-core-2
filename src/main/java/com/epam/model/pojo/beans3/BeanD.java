package com.epam.model.pojo.beans3;

import org.springframework.stereotype.Component;

@Component
public class BeanD {
    private String name;
    private int value;
    public void BeanD() {
        System.out.println("BeanD created");
    }

    public BeanD(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanD{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
