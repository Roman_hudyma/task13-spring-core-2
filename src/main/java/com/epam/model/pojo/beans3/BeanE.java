package com.epam.model.pojo.beans3;

import org.springframework.stereotype.Component;

@Component
public class BeanE {
    private String name;
    private int value;

    public void BeanE() {
        System.out.println("BeanE created");
    }

    public BeanE(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
