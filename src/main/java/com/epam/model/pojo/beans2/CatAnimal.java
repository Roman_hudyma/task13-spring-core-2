package com.epam.model.pojo.beans2;

import org.springframework.stereotype.Component;

@Component
public class CatAnimal {
    private String name;
    private String breed;

    public void CatAnimal() {
        System.out.println("Meeeeeeewwww!!!");
    }

    public CatAnimal(String name, String breed) {
        this.name = name;
        this.breed = breed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    @Override
    public String toString() {
        return "CatAnimal{" +
                "name='" + name + '\'' +
                ", breed='" + breed + '\'' +
                '}';
    }
}
