package com.epam.model.pojo.beans2;

import org.springframework.stereotype.Component;

@Component
public class RoseFlower {
    private String name;
    private String species;
    public void RoseFlower() {
        System.out.println("THIS IS  Rose Flower");
    }

    public RoseFlower(String name, String species) {
        this.name = name;
        this.species = species;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    @Override
    public String toString() {
        return "RoseFlower{" +
                "name='" + name + '\'' +
                ", species='" + species + '\'' +
                '}';
    }
}
