package com.epam.model.pojo.other;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
@Qualifier("FIELD")
public class OtherBeanA {
    private int value;
    public OtherBeanA()
    {
        System.out.println("Autowiring by field");
    }


    public OtherBeanA(int value) {
        this.value = value;
}
    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
