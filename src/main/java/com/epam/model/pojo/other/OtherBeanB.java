package com.epam.model.pojo.other;

import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Order(3)
public class OtherBeanB {
    private int value;
    public OtherBeanB() {
        System.out.println("Autowiring by setter");
    }

    public OtherBeanB(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }


}
