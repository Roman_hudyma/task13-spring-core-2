package com.epam.model.pojo.other;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
public class OtherBeanC {
    private int value;
    public OtherBeanC() {
        System.out.println("Autowiring by constructor");
    }

    public OtherBeanC(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "OtherBeanC{" +
                "value=" + value +
                '}';
    }
}
