package com.epam.model.pojo.other;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class BeanGenerator {
    private OtherBeanB beanB;
    private OtherBeanC beanC;
    @Autowired
    @Qualifier("FIELD")
    private OtherBeanA beanA;

    @Autowired
    public void setBeanB(OtherBeanB beanB)
    {
        this.beanB=beanB;
    }
    @Autowired
    public  BeanGenerator(OtherBeanC beanC)
    {
        this.beanC=beanC;
    }
}
