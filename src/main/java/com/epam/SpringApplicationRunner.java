package com.epam;

import com.epam.view.MainView;

public class SpringApplicationRunner {
    public static void main(String[] args) {
        new MainView().run();
    }
}
