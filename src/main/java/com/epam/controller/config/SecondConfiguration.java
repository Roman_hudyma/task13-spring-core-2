package com.epam.controller.config;

import com.epam.model.pojo.beans3.BeanE;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(basePackages = "epam.model.beans3",
        excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = BeanE.class))
@ComponentScan(basePackages = "epam.model.beans2",
        excludeFilters = @ComponentScan.Filter(type = FilterType.REGEX, pattern = "^((?!Flower).)*$"))
public class SecondConfiguration {
}
