package com.epam.controller.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.epam.model.cars",
"com.epam.model.pojo.other"})
public class ThirdConfiguration {

}
